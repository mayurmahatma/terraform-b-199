provider "aws" {
region = "us-east-1"
}
resource "aws_s3_bucket" "my_bk" {
  bucket = "mayur99"
  acl = "private"
  tags = {
    Name = "mayur99"
  }
}


resource "aws_iam_policy" "s3_bucket_policy" {
  name        = "S3BucketPolicyForMyBucket"
  description = "IAM policy for granting PutBucketPolicy action on the S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "s3:PutBucketPolicy",
        Resource = "arn:aws:s3:::mayur99"
      }
    ]
  })
}

resource "aws_iam_user" "my_iam_user" {
  name = "AIRTEL5G"
  path = "/"
  tags = {
    Name = "airtel5g"
  }
}

resource "aws_iam_user_policy_attachment" "attachment-policy" {
  user       = aws_iam_user.my_iam_user.name
  policy_arn = aws_iam_policy.s3_bucket_policy.arn
}

output "iam_user_name" {
  value = aws_iam_user.my_iam_user.name
}