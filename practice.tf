provider "aws" {
    region = "us-east-1"
}
resource "aws_instance" "my_instance" {
ami = "ami-0e731c8a588258d0d"
instance_type = "t2.micro"
key_name = "Ovi-Key"
vpc_security_group_ids = ["sg-029def9eeffa561ef"]
tags = {
    Name = "mayur"
    env = "dev"
    }
 }